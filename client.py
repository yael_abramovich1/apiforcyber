# used to send web requests
import requests

# used to test input
import re

class Request:
    def __init__(self):

        self.url = 'http://localhost:5000/'

    def get_all(self):
        '''
        askes for all data
        '''
        r = requests.get(self.url)
        if r.status_code == 200:
            return r.json()
        else:
            return 'error'

    def update(self, sat_name):
        '''
        updates / adds satellite
        '''
        height = input('what hight should the satellite be at? ')
        height = check_var(height)
        gas = input('how much gas does the satellite have? ')
        gas = check_var(gas)
        if gas and height:
            info = {'name': sat_name, 'height': height, 'gas': gas}
            req = requests.post(self.url + 'change/'+sat_name, data = info)
            if req.status_code == 200:
                return 'updated successfully'
            else:  
                return 'error'
        else: 
            return 'error'

    def remove(self, sat_name):
        '''
        sends delete requests to remove satellite
        '''
        req = requests.delete(self.url + 'change/' + sat_name)
        if req.status_code == 200:
            return 'removed successfully'
        else:
            return 'error'


def check_var(var):
    '''
    returns variable if it matches the standard
    '''
    if re.match('^\w*$', var) and len(var) < 20:
        return var
    else:
        return None
     

# continues to deliver menu
while True:

    current_request = Request()

    # printing options
    print('hello! welcome to the satellite management system\n'
    'enter 1 to see all  satellites\nenter 2 to change satellite'
    ' details\nenter 3 to add\nenter 4 to remove\nenter 5 to exit')

    # asks for user's choice
    choice = input('what would you like to do? ')

    # choice 1 - prints satellites
    if choice == '1':
        all_satellites = current_request.get_all()
        for satellite in all_satellites:
            print(satellite)
        

    # choice 2 - used to update satellites
    elif choice == '2':
        all_satellites = str(current_request.get_all())
        sat_name = input('enter sattellite name: ')
        sat_name = check_var(sat_name)
        if sat_name:
            if sat_name in all_satellites:
                response = current_request.update(sat_name)
                print(response)
            else:
                print ('satellite does not exist')
        else:
            print('error')
         

    # chice 3 - used to add satellites
    elif choice == '3':
        all_satellites = current_request.get_all()
        if len(all_satellites) >= 10:
            print ('satellite limit met, cannot add more')
        else:
            sat_name = input('enter sattellite name: ')
            sat_name = check_var(sat_name)
            if sat_name:
                response = current_request.update(sat_name)
                print (response)
            else:
                print('error')
         

    # choice 4 - used to remove satellites
    elif choice == '4':
        all_satellites = str(current_request.get_all())
        sat_name = input('enter sattellite name: ')
        sat_name = check_var(sat_name)
        if sat_name:
            if sat_name in all_satellites:
                response = current_request.remove(sat_name)
                print(response)
            else:
                print ('satellite does not exist')
        else:
            print('error')
         

    # choice 5 - exit
    elif choice == '5':
        break

    
    else:
        input('Invalid choice')

    input('continue...\n')

