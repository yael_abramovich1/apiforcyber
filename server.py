# used to start connection with mongo db
import pymongo

# used for web api
from flask import Flask, jsonify, request

# used for variable testing
import re

def find_collection():
    '''
    returns mongodb collection
    '''

    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client['local']

    if 'satellite' not in db.list_collection_names():
        db.create_collection('satellite')

    collection = db['satellite']
    return collection


def check_var(var):
    '''
    retrun secure var only
    '''

    if re.match('^\w*$', var) and len(var)<20:
        return var
    else:
        return None

# starts web api
app = Flask(__name__)

@app.route('/')
def show_all():
    '''
    returns all satellites
    '''
    document_list =[]
    collection = find_collection()
    for document in collection.find({}, {'_id': 0 }):
        document_list.append(str(document))
    return jsonify(document_list)

@app.route('/change/<string:name>', methods = ['POST', 'DELETE'])
def add_sat(name):
    '''
    modifies, adds, or removes satellites
    '''
    collection = find_collection()

    # delets one satellite
    if request.method == 'DELETE':
        collection.delete_one({'name': name})

    # modifies or addes a satellite
    elif request.method == 'POST':
        gas = request.form['gas']
        gas = check_var(gas)
        height = request.form['height']
        height = check_var(height)
        collection.update({'name': name}, {'name': name, 'height': height, 'gas': gas}, upsert=True)

    return 'Changed successfully'

if __name__ == '__main__':
    app.run()
